package com.damonprovider.dubboprovider;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDubbo //采用com.alibaba.dubbo一定不能少了这个注解，开启dubbo服务，也可以通过properties文件配置
@SpringBootApplication
public class DubboproviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(DubboproviderApplication.class, args);
	}

}

