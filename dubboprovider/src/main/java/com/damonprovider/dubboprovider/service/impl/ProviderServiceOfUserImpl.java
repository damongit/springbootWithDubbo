package com.damonprovider.dubboprovider.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.damon.commonapi.service.ProviderServiceOfUser;
import org.springframework.stereotype.Component;

@Component
@Service(version = "1.0.0") //这是是dubbo的注解，千万要注意，意思是暴露服务,version可有可无，是一个标记
/**
 * 这个接口，ProviderServiceOfUser，是在common-api中的，它其实属于provider，
 * 但是因为consumer需要把该接口注入它的业务类，因此把该接口放到common-api中，
 * 然后在这里实现该接口
 */
public class ProviderServiceOfUserImpl implements ProviderServiceOfUser {
    @Override
    public String getUserName() {
        return "Damon";
    }
}
