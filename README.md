# springbootWithDubbo

#### 介绍
springboot整合dubbo的一个demo
本工程包含3个子模块，一个consumer,一个provider,一个common-api。
common-api存放的是consumer和provider都需要使用的接口。
RPC，远程过程调用，那么就是说，consumer可以远程调用provider中的方法，那就需要在consumer中注入provider中的接口，
那么就把provider中的接口剥离出来，放到common-api中，provider中存放的是这些接口的实现类，然后consumer的pom.xml中引入common-api，
这样的话consumer就可以注入common-api的接口，但实际上在provider中有实现类，实际参与业务的就是这个实现类。
#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)