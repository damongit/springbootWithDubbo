package com.damonconsumer.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.damon.commonapi.service.ProviderServiceOfUser;
import com.damonconsumer.service.ConsumerServiceOfUser;
import org.springframework.stereotype.Component;

@Component
@Service //这是dubbo的注解
public class ConsumerServiceOfUserImpl implements ConsumerServiceOfUser {

    @Reference(version = "1.0.0") //在provider中加了version标记，那么这里也得写
    private ProviderServiceOfUser providerServiceOfUser;


    @Override
    public String getUserNameFromProvider() {
        return providerServiceOfUser.getUserName();
    }
}
