package com.damonconsumer.service.controller;

import com.damonconsumer.service.ConsumerServiceOfUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/consumer")
public class ConsumerControllerOfUser {

    @Autowired
    private ConsumerServiceOfUser consumerServiceOfUser;

    @GetMapping(value = "/getusername")
    public String getUserName() {
        return consumerServiceOfUser.getUserNameFromProvider();
    }
}
