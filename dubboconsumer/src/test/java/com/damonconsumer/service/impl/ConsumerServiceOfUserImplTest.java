package com.damonconsumer.service.impl;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@WebAppConfiguration
@EnableDubbo
public class ConsumerServiceOfUserImplTest {

    @Autowired
    private ConsumerServiceOfUserImpl consumerServiceOfUser;

    @Test
    public void test1() {
        String name = consumerServiceOfUser.getUserNameFromProvider();
        System.out.println("name=" + name);
    }


}