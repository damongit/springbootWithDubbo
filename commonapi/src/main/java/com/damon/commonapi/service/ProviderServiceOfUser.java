package com.damon.commonapi.service;

public interface ProviderServiceOfUser {

    String getUserName();
}
